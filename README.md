### Installation instructions
_Tested on Windows 10 and Linux Mint_
- Download Node LTS from your platform from https://nodejs.org/en/

 Alternatively your package manager may have npm or node packages. 
Debian distros can run ``sudo apt-get install npm`` (Tested on Linux Mint)
 
- Open a terminal in the root folder containing package.json and run the following

```
npm install -g yarn # On linux need to sudo as it's a global package
yarn install # If you get an error about incompatible node on linux edit package.json instructions below
npm run build:prod # note the colon is very important you will get an error without it 
npm link # Need to sudo on linux -creates a symlink to the command, if you see an error check the build ran successfully
```

*package.json notes* 

if your node version is older than specified (10.13) then you can alter 
the "engines": { "node": ">= 10.13} entry in package.json, matching 10.13 
with your version. Note I can't guarantee the program will work but it's 
an option if you can't get 10.13 or above. 8.10 appeared to run fine for 
me in testing

Once the link command completes successfully you can run `wpe_merge` from the terminal

##### A note about yarn
Yarn is technically no longer necessary for projects since npm 5 added package-lock 
but it fetches packages in parallel and package-lock is still not quite as 
reliable as yarn.lock which is why I have chosen to use it.

### Usage
`wpe_merge path/to/input-file path/to/output-file`

Note that no assumptions are made about the file extension, 
if you run with your input file as `input` then the command assumes you mean the file literally named input, 
it will ignore input.csv in the same location

To facilitate automated tests output files will be overwritten without warning
so please ensure you don't overwrite anything accidentally 

#### File format

By default the provided file must have a header line consisting of 
``Account ID,Account Name,First Name,Created On`` in that exact order. 
If the order is incorrect the command assumes the file is formatted incorrectly.

Note that the header line is case insensitive and extra whitespace around commas is ignored 

The data must also follow this layout format or strange things may happen.

#### Missing Data
If a record in the input file is missing the primary key ( Account ID ) then 
it will be skipped with a warning and processing will continue

If a record is missing any other data then the existing data will be copied 
to the file with empty fields between commas ( eg 1234,,)

If a record cannot be found in the REST Api a warning will be shown and the data 
from the input file will still be merged to the final output file   

### Unit tests
``npm test``

Note - Jest uses mocks which are found in adjacent \__mocks__ folders to the real modules.
The top level \__mocks__ folder is used to mock node_modules 

### Linting
``npm run lint``

#### Customising
It's probably not required but if you wish you can reconfigure the fields that are expected
in the input file and the fields to output to the output.csv as well as rest api mappings and
fields to retrieve from the rest endpoint in src/main/config/data-format.ts (you will need to 
rebuild the app or alternatively edit the compiled js directly at dist/config/data-format.js


### Cleanup
When you have finished running the script you can remove the symlink from your system 
by running ``npm unlink`` from the root folder containing package.json

## 3rd party libraries used
- request-promise-native - Uses native ES6 promises with request package
- request - Simplified http client
- csv - CSV parser suite 
- jest - JS testing framework
- streamtest - convenience package for testing npm streams
- tslint - code style rules
- tslint-microsft-contrib - code style rules from MS
- rimraf - os agnostic rm -rf for cleanup
- Typescript, utilities and typings to support Typescript

## A note on code documentation
I have used JSDoc as I feel it is expected, however my goal is always to make the code
as self-documenting as possible with good function and variable names. I feel like in 
this case a lot of the JSdoc is redundant and adds visual clutter but that may be
due to my inexperience writing jsdoc and I hope it will not stand against me.

## Why Node & Typescript? 
I think node was an excellent fit for this project due to the simple file parsing, http calls
and data manipulation and the availability of so many useful plugins. However in my experience 
Node projects can quickly become unwieldy even with ES6 classes so I added Typescript to try and 
enforce some structure 