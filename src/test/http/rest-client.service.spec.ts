/* tslint:disable */
import {RestClientService} from "../../main/http/rest-client.service"

jest.mock('request-promise-native');

let restClient: RestClientService;

describe('RestClientService tests', () => {
    beforeEach(() => {
        restClient = new RestClientService('https://fake.endpoint.com');
    });

    it('should try to get {baseUrl}/relativePath', async () => {
        const response = await restClient.get('my-fake-endpoint/resources');
        expect(response.uri).toEqual('https://fake.endpoint.com/my-fake-endpoint/resources');
    });

    it('should deal with relativePath having a leading /', async () => {
        const response = await restClient.get('/my-fake-endpoint/resources');
        expect(response.uri).toEqual('https://fake.endpoint.com/my-fake-endpoint/resources');
    });

    it('404 error should resolve as "404" instead of rejecting', async () => {
        const response = await restClient.get('404');
        expect(response).toEqual('404');
    });

    it('403 error should re-throw', async () => {
        try {
            await restClient.get('403');
            //make sure the error is thrown by having a fail case
            expect(true).toBeFalsy();
        } catch (err) {
            expect(err.statusCode).toEqual(403);
        }
    });

    it('Non JSON endpoint should reject', async() => {
        try {
            await restClient.get('text/html');
            expect(true).toBeFalsy();
        } catch(err) {
            expect(err).toEqual('URL was not a valid JSON endpoint');
        }
    });

    it('Invalid JSON should reject', async() => {
        try {
            await restClient.get('badjson');
            expect(true).toBeFalsy();
        } catch(err) {
            expect(err).toEqual('A JSON endpoint was found at https://fake.endpoint.com/badjson but the json parser failed with the following error - SyntaxError: Unexpected end of JSON input');
        }
    });

    it('Empty valid JSON should not reject', async() => {
        try {
            const response = await restClient.get('emptyjson');
            expect(response).toEqual({});
        } catch(err) {
            expect(true).toBeFalsy()
        }
    });


});