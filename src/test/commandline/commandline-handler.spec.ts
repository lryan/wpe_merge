import {CommandLineHandler} from "../../main/commandline/commandline-handler";
import {WpeMerge} from "../../main/processing/wpe-merge";

jest.mock('../../main/processing/wpe-merge');

describe('CommandLineHandler deals with arguments correctly', () => {
    beforeEach(() => {
            jest.spyOn(console, "log").mockImplementation(()=>{});
        }
    );

    it('Should print usage for 0 arguments', () => {
        CommandLineHandler.RUN([]);
        expect(console.log).toHaveBeenCalledWith(CommandLineHandler.usage);
    });

    it('Should print usage for 1 valid argument', () => {
        CommandLineHandler.RUN(['input.csv']);
        expect(console.log).toHaveBeenCalledWith(CommandLineHandler.usage);
    });

    it('Should call WpeMerge MERGE_WITH_REST_DATA with valid arguments', () => {
        CommandLineHandler.RUN(['input', 'output']);
        expect(WpeMerge.MERGE_WITH_REST_DATA).toHaveBeenCalledWith('input', 'output');
    });

    it('Should log usage for empty arguments', () => {
        CommandLineHandler.RUN(['', '']);
        expect(console.log).toHaveBeenCalledWith(CommandLineHandler.usage);
    });

    it('Should log usage for whitespace arguments', () => {
        CommandLineHandler.RUN(['  ', '    ']);
        expect(console.log).toHaveBeenCalledWith(CommandLineHandler.usage);
    })

});