import {CsvParseStreamFactory} from "../../../main/streams/factories/csv-parse-stream.factory"
import * as StreamTest from 'streamtest';

const streamTest = StreamTest['v2'];

const csvTestData = {
    happy: [`Account ID, Account Name, First Name, Created On\r\n1234, Nigma Enterprises, Edward, 2015-03-05`],
    case_insensitive: [`account id, account name, FirSt NaMe, CREATED On\r\n1234, Nigma Enterprises, Edward, 2015-03-05`],
    whitespace: [`account id  ,  account name  , FirSt NaMe, CREATED On\r\n1234, Nigma Enterprises, Edward, 2015-03-05`],
    out_of_order: [`Account ID, First Name, Account Name, Created On\r\n1234, Edward, Nigma Enterprises, 2015-03-05`],
    wrong_data: [`name, alter-ego\r\nPeter Parker, Spiderman`],
    empty_line: [`account id  ,  account name  , FirSt NaMe, CREATED On\r\n   `, `\r\n`, `1234, Nigma Enterprises, Edward, 2015-03-05`],
    error_line: [`account id  ,  account name  , FirSt NaMe, CREATED On\r\n`, `\r\n`, `1234, Nigma Enterprises, Edward, 2015-03-05`,
        '\r\nthis,is,not,the,correct,data']
};

// @ts-ignore
const mockExit = jest.spyOn(process, 'exit').mockImplementation(() => {
});

const expectedOutputs = {
    good: {
        'Account ID': '1234',
        'Account Name': 'Nigma Enterprises',
        'Created On': '2015-03-05',
        'First Name': 'Edward'
    }
};

function testStreamWithInput(chunk: string[], expectedOutput: Object, done: any) {
    let csvStream = CsvParseStreamFactory.GET_INSTANCE().createErrorHandlingStream();
    streamTest.fromChunks(chunk)
        .pipe(csvStream)
        .pipe(streamTest.toObjects((err: Error, obj: object[]) => {
            if (err) {
                done(err);
            }
            expect(obj[0]).toEqual(expectedOutput);
            done()
        }));
}

function streamShouldExitProcess(chunk: string[], done: any) {
    jest.spyOn(console, 'error').mockImplementation(() => {
    });
    jest.spyOn(console, 'info').mockImplementation(() => {
    });
    let csvStream = CsvParseStreamFactory.GET_INSTANCE().createErrorHandlingStream();
    streamTest.fromChunks(chunk)
        .pipe(csvStream);
    csvStream.on('close', () => {
        expect(mockExit).toHaveBeenCalledWith(1);
        done();
    })
}

describe('CsvParseStream tests', () => {

    it('Should pass parsed object if headers are correct', (done: jest.DoneCallback) => {
        testStreamWithInput(csvTestData.happy, expectedOutputs.good, done);
    });

    it('Should ignore case problems in input file and fix for output', (done: jest.DoneCallback) => {
        testStreamWithInput(csvTestData.case_insensitive, expectedOutputs.good, done);
    });

    it('Should ignore extra whitespace around correct fields in input and fix for output', (done: jest.DoneCallback) => {
        testStreamWithInput(csvTestData.whitespace, expectedOutputs.good, done);
    });

    it('Should throw an error if fields are wrong order', (done: jest.DoneCallback) => {
        streamShouldExitProcess(csvTestData.out_of_order, done);
    });

    it('Should throw an error if fields are incorrect', (done: jest.DoneCallback) => {
        streamShouldExitProcess(csvTestData.wrong_data, done);
    });

    it('Should skip empty lines silently', (done: jest.DoneCallback) => {
        jest.spyOn(console, 'warn').mockImplementation(() => {});
        let csvStream = CsvParseStreamFactory.GET_INSTANCE().createErrorHandlingStream();
        streamTest.fromChunks(csvTestData.empty_line)
            .pipe(csvStream)
            .pipe(streamTest.toObjects((err: Error, obj: object[]) => {
                if (err) {
                    done(err);
                }
                expect(console.warn).toBeCalledTimes(0);
                expect(obj[0]).toEqual(expectedOutputs.good);
                expect(obj.length).toEqual(1);
                done()
            }));
    });

    it('Should warn and skip error lines', (done: jest.DoneCallback) => {
        jest.spyOn(console, 'warn').mockImplementation(() => {});
        let csvStream = CsvParseStreamFactory.GET_INSTANCE().createErrorHandlingStream();
        streamTest.fromChunks(csvTestData.error_line)
            .pipe(csvStream)
            .pipe(streamTest.toObjects((err:Error, obj: object[]) => {
                if (err) {
                    done(err);
                }
                expect(console.warn).toHaveBeenCalledWith('An error was detected in line 4, this record will be skipped');
                expect(obj[0]).toEqual(expectedOutputs.good);
                expect(obj.length).toEqual(1);
                done()
            }));
    })


});