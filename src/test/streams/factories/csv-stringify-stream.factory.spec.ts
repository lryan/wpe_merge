import {CsvStringifyStreamFactory} from "../../../main/streams/factories/csv-stringify-stream.factory"
import * as StreamTest from 'streamtest';

const streamTest = StreamTest['v2'];

// @ts-ignore
const mockExit = jest.spyOn(process, 'exit').mockImplementation(() => {});
const dataToStringify = {
    good: [{
        'Account ID': '1234',
        'Account Name': 'Nigma Enterprises',
        'First Name': 'Edward',
        'Created On': '2015-03-05',
    }]
};

let csvStream;

describe('CsvStringifyStream tests', () => {

    beforeEach(() => {
        csvStream = CsvStringifyStreamFactory.GET_INSTANCE().createErrorHandlingStream();
        jest.spyOn(console, 'warn').mockImplementation(() => {});
        jest.spyOn(console, 'error').mockImplementation(() => {});
        jest.spyOn(console, 'log').mockImplementation(() => {});
    });

    it('Should output correct format', (done: jest.DoneCallback) => {
        streamTest.fromObjects(dataToStringify.good)
            .pipe(csvStream)
            .pipe(streamTest.toText((err: Error, result: string) => {
                if (err) {
                    done(err);
                }
                expect(console.warn).toBeCalledTimes(0);
                expect(result).toEqual(`Account ID,Account Name,First Name,Created On\n1234,Nigma Enterprises,Edward,2015-03-05\n`);
                done()
            }));
    });

    it('should exit process on error', (done: jest.DoneCallback) => {
        let csvStream = CsvStringifyStreamFactory.GET_INSTANCE().createErrorHandlingStream();
        csvStream.on('close', () => {
            expect(mockExit).toHaveBeenCalledWith(1);
            done();
        });
        csvStream.emit('error', new Error('An unexpected error occured'));

    })

});