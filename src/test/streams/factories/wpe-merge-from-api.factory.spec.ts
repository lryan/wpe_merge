import {WpeMergeFromApiFactory} from "../../../main/streams/factories/wpe-merge-from-api.factory"

// @ts-ignore
const mockExit = jest.spyOn(process, 'exit').mockImplementation(() => {});

describe('CsvStringifyStream tests', () => {

    beforeEach(() => {
       jest.spyOn(console, 'error').mockImplementation(()=>{});
       jest.spyOn(console, 'log').mockImplementation(()=>{});
       jest.spyOn(console, 'info').mockImplementation(()=>{});
    });
    it('should exit process on error', (done: jest.DoneCallback) => {
        let wpeStream = WpeMergeFromApiFactory.GET_INSTANCE().createErrorHandlingStream();
        wpeStream.on('close', () => {
            expect(mockExit).toHaveBeenCalledWith(1);
            expect(console.error).toHaveBeenCalledWith('Holy unexpected error Batman!');
            done();
        });
        wpeStream.emit('error', new Error('An unexpected error occured'));
    })

});