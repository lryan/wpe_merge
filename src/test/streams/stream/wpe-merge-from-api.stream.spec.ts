import {WpeMergeFromApiStream} from "../../../main/streams/stream/wpe-merge-from-api.stream";
import {DataFormat} from "../../../main/config/data-format";

const csvRecordPrimaryKey = DataFormat.csvRecordPrimaryKey;

jest.mock('../../../main/http/rest-client.service');
let stream: WpeMergeFromApiStream;

const testData = {
    happy: {
        "Account ID": 4213,
        "Account Name": 'Cobblepot',
        "First Name": 'Oswald',
        "Created On": '2013-02-5'
    },
    sad: {
        "Account ID": 'mia'
    },
    server404: {
        "Account ID": 'server404',
    }
};

let result = {
    error: '',
    data: null
};

const setResult = (error: any, data: any) => {
    result = {
        error: error,
        data: data
    }
};

describe('WpeMergeFromApiStream tests', () => {
    beforeEach(() => {
        stream = new WpeMergeFromApiStream();
        setResult('', '');
    });

    it('Should merge correctly when primary key exists', async () => {
        await stream._transform(testData.happy, null, setResult);
        expect(result.data).toEqual({
            "Account ID": 4213,
            "First Name": 'Oswald',
            "Created On": '2013-02-5',
            "Status": 'good',
            "Status Set On": '2014-03-03'
        });

        expect(result.error).toBeFalsy();
    });

    it('Should print a warning but not throw an error if 404 encountered', async () => {
        spyOn(console, 'warn');
        await stream._transform(testData.server404, null, setResult);

        expect(console.warn).toHaveBeenCalledWith(
            `Warning - record for ${csvRecordPrimaryKey} ${testData.server404[`${csvRecordPrimaryKey}`]} not found at`);
        expect(result.data).toEqual({"Account ID": "server404", "Status": "", "Status Set On": ""});
        expect(result.error).toBeFalsy();
    });

    it('Should print a warning but not throw an error if empty response - should not happen but may be recoverable for a bad api record', async () => {
        spyOn(console, 'warn');
        await stream._transform(testData.sad, null, setResult);

        expect(console.warn).toHaveBeenCalledWith(
            `Warning - record for ${csvRecordPrimaryKey} ${testData.sad[`${csvRecordPrimaryKey}`]} not found at`);
        expect(result.data).toEqual({"Account ID": "mia", "Status": "", "Status Set On": ""});
        expect(result.error).toBeFalsy();
    });

});