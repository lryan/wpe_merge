interface FileStreamOptions extends Object {
    filePath?: string;
    isWriteStream?: boolean;
    //node write and read stream properties from here down
    flags?: string;
    encoding?: string;
    fd?: number;
    mode?: number;
    autoClose?: boolean;
    start?: number;
    end?: number;
    highWaterMark?: number;
}