import {Stream} from "stream";

/**
 * Interface is preferable to inheritance as the code can be made more modular with future requirements
 */
export interface StreamFactoryInterface {
    createErrorHandlingStream: (options?: object) => Stream
}