#!/usr/bin/env node
import {CommandLineHandler} from "../commandline/commandline-handler";

/**
 * This file is JS to work around compiled TS files not respecting the shebang above ( Microsoft say this is not a bug )
 */

const userSuppliedArgs = process.argv.slice(2);
CommandLineHandler.RUN(userSuppliedArgs);