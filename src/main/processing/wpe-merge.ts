import {FileStreamFactory} from "../streams/factories/file-stream.factory";
import {CsvParseStreamFactory} from "../streams/factories/csv-parse-stream.factory";
import {WpeMergeFromApiFactory} from "../streams/factories/wpe-merge-from-api.factory";
import {CsvStringifyStreamFactory} from "../streams/factories/csv-stringify-stream.factory";

/**
 * Main processing class which instantiates all of the streams and sets up the pipe with a little message at start and
 * end of processing. Needs a better name but my brain is blocked on it
 */
export class WpeMerge {

    /**
     *  Main processing
     *
     * @param {string} inputFilePath
     * @param {string} outputFilePath
     * @constructor
     */
    public static MERGE_WITH_REST_DATA(inputFilePath: string, outputFilePath: string): void {
        const fileReadStream = FileStreamFactory.GET_INSTANCE().createErrorHandlingReadStream(inputFilePath);
        const fileWriteStream = FileStreamFactory.GET_INSTANCE().createErrorHandlingWriteStream(outputFilePath);
        const csvParseStream = CsvParseStreamFactory.GET_INSTANCE().createErrorHandlingStream();
        const csvStringifyStream = CsvStringifyStreamFactory.GET_INSTANCE().createErrorHandlingStream();
        const apiMergingStream = WpeMergeFromApiFactory.GET_INSTANCE().createErrorHandlingStream();
        console.log(`Generating file ${outputFilePath} based on the data in ${inputFilePath}`);

        fileReadStream
            .pipe(csvParseStream)
            .pipe(apiMergingStream)
            .pipe(csvStringifyStream)
            .pipe(fileWriteStream);

        fileWriteStream.on('finish', () => {
            console.log(`Operation completed`);
        })
    }

}
