/**
 * Data format for the csv file and api. By modifying properties here we can easily adjust to changes in format
 * As I'm coding in Node / TS I prefer a TS config file over json in case we need to get some properties dynamically
 */
export const DataFormat = {
    csvFileHeaders: {
        inputFileHeaders: ['Account ID', 'Account Name', 'First Name', 'Created On'],
        outputFileHeaders: ['Account ID', 'First Name', 'Created On', 'Status', 'Status Set On']
    },
    apiProperties: {
        keysToRetrieve: ['status', 'created_on'],
        mapping: {
            status: 'Status',
            created_on: 'Status Set On'
        },
        endpointRelativePath: '/accounts/'
    },
    csvRecordPrimaryKey: 'Account ID'
};
