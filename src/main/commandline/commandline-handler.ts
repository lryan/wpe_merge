import {WpeMerge} from '../processing/wpe-merge';

/**
 * Check that arguments are valid and hand off to the processor.
 */
export class CommandLineHandler {

    static usage: string =
        `Usage wpe_engine path/to/inputfile path/to/outputfile\r\neg wpe_engine myFile.csv output.csv`;

    /**
     * Calls WpeMerge to initiate processing or prints usage if args are invalid
     *
     * @param {string[]} args - User supplied arguments
     */
    static RUN(args: string[]): void {
        if (CommandLineHandler.argsAreValid(args)) {
            WpeMerge.MERGE_WITH_REST_DATA(args[0], args[1]);
        } else {
            console.log(CommandLineHandler.usage);
        }
    }

    /**
     * Checks for two non-empty arguments. Note that things like '' or "" will be seen as valid but read errors should
     * exit the process if the file does not exist in the filesystem
     *
     * @param {string[]} args - User supplied arguments
     * @returns {boolean}
     */
    private static argsAreValid(args: string[]) {
        if (args.length !== 2) {
            return false;
        }
        for (let arg of args) {
            if (!arg.trim()) {
                return false
            }
        }
        return true;
    }
}