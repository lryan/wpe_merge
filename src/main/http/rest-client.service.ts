import {get} from 'request-promise-native';

/**
 * Basic client used for GETting json endpoints
 */
export class RestClientService {
    readonly endpointBaseUrl: string;

    /**
     *
     * @param {string} endPointBaseUrl Url from which all relative paths are based
     */
    constructor(endPointBaseUrl: string) {
        this.endpointBaseUrl = endPointBaseUrl;
    }

    /**
     * @param {string} relativePath relative path to the resource on the server
     * @returns {Promise} see fetchJson for more info
     */
    public get(relativePath: string) {
        if (relativePath.charAt(0) === '/') {
            relativePath = relativePath.slice(1);
        }
        return this.fetchJson(`${this.endpointBaseUrl}/${relativePath}`)
    }

    /**
     * Retrieve a provided url and automatically parse to JSON. Returns an error if the content is not JSON or if parsing fails
     *
     * @param url - the url to query
     * @return Promise
     *      resolves with an object if valid json endpoint
     *      !IMPORTANT resolves with '404' if the endpoint does not exist on the server
     *      rejects with error message for other issues
     */
    public fetchJson(url: string) {
        return this.fetchUri(url)
            .then((response: any) => {
                let contentTypeHeaders = response.headers['content-type'];
                if (response.statusCode === 200 && contentTypeHeaders && contentTypeHeaders.toLowerCase().includes('application/json')) {
                    try {
                        return Promise.resolve(JSON.parse(response.body));
                    } catch (error) {
                        return Promise.reject(`A JSON endpoint was found at ${url} but the json parser failed with the following error - ${error}`)
                    }
                }
                return Promise.reject("URL was not a valid JSON endpoint")
            })
            .catch((reason: any) => {
                if (reason.statusCode === 404) {
                    return Promise.resolve('404');
                }
                //Assuming that a 401, 403 or similar error should just halt the program since it will most likely apply to all endpoints
                throw reason;//out the window :) - will be caught by stream error handler and cause process to exit
            })
    };

    /**
     * Retrieve a provided uri
     *
     * @param uri the full uri as a string
     * @returns {Promise} Promise which resolves to a request or a rejection
     */
    private fetchUri(uri: string) {
        let options = {
            uri: uri,
            resolveWithFullResponse: true
        };
        return get(options);
    };

}
