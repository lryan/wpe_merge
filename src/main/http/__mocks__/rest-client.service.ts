/**
 * Jest mock class for unit tests
 */
export class RestClientService {
    apiData: object = {
        4213: {
            account_id: 4213,
            status: 'good',
            created_on: '2014-03-03'
        },
        server404: '404'
    };

    public get(relativePath: string) {
        const id = relativePath.substr(relativePath.lastIndexOf('/') + 1);
        return Promise.resolve(this.apiData[id]);
    }
}
