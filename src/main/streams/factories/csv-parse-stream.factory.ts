import * as csv from 'csv'
import {WriteStream} from "fs";
import {DataFormat} from "../../config/data-format";
import {StreamFactoryInterface} from "../../interfaces/stream.factory.interface";

/**
 * Singleton to create csv parser streams with error handling
 */
export class CsvParseStreamFactory implements StreamFactoryInterface {
    private static instance: CsvParseStreamFactory;

    private constructor() {}

    static GET_INSTANCE() {
        if (!CsvParseStreamFactory.instance) {
            CsvParseStreamFactory.instance = new CsvParseStreamFactory();
        }
        return CsvParseStreamFactory.instance;
    }

    private defaultStreamOptions: csv.options = {
        columns: this.ensureCorrectInputFileHeaders,
        skip_empty_lines: true,
        skip_lines_with_error: true,
        trim: true,
    };

    /**
     * This function is provided to the defaultStreamOptions to ensure the headers are valid and in the correct order
     *
     * @param {string[]} headers
     *
     * @returns {string[]}
     */
    private ensureCorrectInputFileHeaders(headers: string[]) {
        const knownGoodHeaders = DataFormat.csvFileHeaders.inputFileHeaders;
        headers.forEach((header: string, index: number) => {
            header = header.trim().toLowerCase();
            if (header === knownGoodHeaders[index].toLowerCase()) {
                headers[index] = knownGoodHeaders[index];
            } else {
                throw new Error(`First line of input file must contain the headers ${knownGoodHeaders.join()}\r\nYour file has ${headers.join()}`);
            }
        });
        return headers;
    }

    /**
     * Get a preconfigured stream with built-in error handling
     *
     * @param {object} options
     * @returns {WriteStream}
     */
    public createErrorHandlingStream(options: object = {}): WriteStream {
        const csvParseStream = csv.parse(Object.assign(options, this.defaultStreamOptions));
        this.setErrorHandler(csvParseStream);
        return csvParseStream;
    }

    /**
     * Convenience method to keep the code readable
     *
     * @param stream
     */
    private setErrorHandler(stream: any): void {

        stream.on('error', (err: Error) => {
            console.error(`File format problem on line ${stream.info.lines}.\r\n${err}`);
            console.info(`Expected format is ${DataFormat.csvFileHeaders.inputFileHeaders}\r\n`);
            stream.destroy();
            process.exit(1);
        });

        stream.on('skip', (skipReason: Error) => {
            console.warn(`An error was detected in line ${stream.info.lines}, this record will be skipped`);
            console.warn(`Skip reason - ${skipReason}`)
        })
    }

}