import * as csv from 'csv'
import {WriteStream} from "fs";
import {StreamFactoryInterface} from "../../interfaces/stream.factory.interface";

/**
 * Singleton to create csv stringify streams with error handling
 */
export class CsvStringifyStreamFactory implements StreamFactoryInterface {
    private static instance: CsvStringifyStreamFactory;

    private constructor() {}

    static GET_INSTANCE() {
        if (!CsvStringifyStreamFactory.instance) {
            CsvStringifyStreamFactory.instance = new CsvStringifyStreamFactory();
        }
        return CsvStringifyStreamFactory.instance;
    }

    private defaultStreamOptions: csv.options = {
        header: true
    };

    /**
     * Get a preconfigured stream with built-in error handling
     *
     * @param {object} options
     * @returns {WriteStream}
     */
    public createErrorHandlingStream(options: object = {}): WriteStream {
        const csvStringifyStream = csv.stringify(Object.assign(options, this.defaultStreamOptions));
        this.setErrorHandler(csvStringifyStream);
        return csvStringifyStream;
    }

    /**
     * Convenience method to keep the code readable
     *
     * @param stream
     */
    private setErrorHandler(stream: any): void {
        stream.on('error', async (err: Error) => {
            console.error(`Encountered an error while generating csv from input - ${stream.info.lines}.\r\n${err}`);
            console.error('Aborting wpe_merge...');
            stream.destroy();
            process.exit(1);
        });
    }

}