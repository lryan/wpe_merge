import * as stream from "stream";
import {WpeMergeFromApiStream} from "../stream/wpe-merge-from-api.stream";
import {StreamFactoryInterface} from "../../interfaces/stream.factory.interface";

/**
 * Singleton for creating WpeMergeFromApiStreams ( the core functionality of the script )
 */
export class WpeMergeFromApiFactory implements StreamFactoryInterface {
    private static instance: WpeMergeFromApiFactory;

    private constructor() {}

    static GET_INSTANCE() {
        if (!WpeMergeFromApiFactory.instance) {
            WpeMergeFromApiFactory.instance = new WpeMergeFromApiFactory();
        }
        return WpeMergeFromApiFactory.instance;
    }

    /**
     * Return a stream with error handler baked in
     *
     * @returns {stream.Transform}
     */
    public createErrorHandlingStream(): stream.Transform {
        const wpeMergeStream = new WpeMergeFromApiStream();
        this.setErrorHandler(wpeMergeStream);
        return wpeMergeStream;
    }

    /**
     * Convenience method to keep code readable
     *
     * @param stream
     */
    private setErrorHandler(stream: any): void {
        stream.on('error', async (err: Error) => {
            console.error("Holy unexpected error Batman!");
            console.error(`Seriously though there was an error, the most likely issue was an unexpected response from the server\r\n${err}`);
            console.info(`Were this a production app contact details for bug reports would go here instead of a terrible attempt at humour`);
            stream.destroy();
            process.exit(1);
        });
    }
}