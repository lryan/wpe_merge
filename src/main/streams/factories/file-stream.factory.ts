import {createReadStream, createWriteStream, ReadStream, WriteStream} from "fs";
import {Stream} from "stream";
import {StreamFactoryInterface} from "../../interfaces/stream.factory.interface";

/**
 * Singleton for creating write and read streams. Possibly they could have been separate classes but it felt like
 * too much duplication
 */
export class FileStreamFactory implements StreamFactoryInterface {
    private static instance: FileStreamFactory;

    private constructor() {}

    static GET_INSTANCE() {
        if (!FileStreamFactory.instance) {
            FileStreamFactory.instance = new FileStreamFactory();
        }
        return FileStreamFactory.instance;
    }

    /**
     * Convenience method to create a read stream from a filename without needing to specify isWritestream

     * @param {string} filePath
     * @returns {ReadStream}
     */
    public createErrorHandlingReadStream(filePath: string): ReadStream {
        return this.createErrorHandlingStream({filePath, isWriteStream: false}) as ReadStream;
    }

    /**
     * Convenience method to create a write stream from a filename without needing to specify isWritestream

     * @param {string} filePath
     * @returns {ReadStream}
     */
    public createErrorHandlingWriteStream(filePath: string): WriteStream {
        return this.createErrorHandlingStream({filePath, isWriteStream: true}) as WriteStream;
    }

    /**
     * Create error handling stream from a given filePath allowing passing of more advanced options than convenience methods
     *
     * @param {FileStreamOptions} options valid node options for read / write streams plus filePath and
     * @returns {Stream}
     */
    public createErrorHandlingStream(options: FileStreamOptions = {filePath: '', isWriteStream: false}): Stream {
        const stream =
            options.isWriteStream ? createWriteStream(options.filePath, options) : createReadStream(options.filePath, options);

        stream.on('error', (err: Error) => {
            console.error(`Unable to open ${options.filePath}. Is the path correct and do you have permission to access it?`);
            console.error(`Technical error is ${err}`);
            stream.destroy();
            process.exit(1);
        });
        return stream;
    }
}