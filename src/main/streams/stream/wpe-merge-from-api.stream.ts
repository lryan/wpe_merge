import * as stream from "stream";
import {RestClientService} from "../../http/rest-client.service";
import {DataFormat} from "../../config/data-format";

/**
 * This transformation stream will take valid 'wpe' objects and query the api for them in the _transform method,
 * merging or handling errors as appropriate ( or at least that's the intention! )
 *
 * There's some ugly tslint and ts-ignore comments in this file but I did not want to disable the rules globally
 */
export class WpeMergeFromApiStream extends stream.Transform {
    private wpeApi: RestClientService;

    /**
     * The stream is made to deal with objects so force objectMode for the stream. It would be relatively straightforward
     * to also handle chunks in the future but Mr. Scope has been creeping
     *
     * @param options
     */
    constructor(options: any = {}) {
        super(Object.assign(options, {objectMode: true}));
        // tslint:disable-next-line no-http-string - interview.wpengine.io has an invalid cert name and tslint hates http
        this.wpeApi = new RestClientService(options.restEndpoint || 'http://interview.wpengine.io/v1');
    }

    /**
     * Implementation of Stream._transform required by all TransformStreams
     *
     * @param csvInputData - the csvInputData being pushed from our source. Technically can be chunks or objects but we only use objects
     * @param {string} encoding - encoding of the string / chunk, unused in object-mode so we ignore it
     * @param {(error?: Error, csvInputData?: any) => void} next - our callback once we successfully process csvInputData, setting
     * the first argument will throw an error down the pipe which we don't want
     *
     */
    // @ts-ignore-next-line - we force object mode in the constructor which ignores the encoding variable, ts is not a fan
    // tslint:disable-next-line function-name - need method name to be called _transform for override, tslint doesn't like it
    _transform(csvInputData: any, encoding: string, next: (error?: Error, data?: any) => void) {
        const primaryKey = DataFormat.csvRecordPrimaryKey;
        let primaryKeyValue;

        try {
            primaryKeyValue = csvInputData[primaryKey];
        } catch (ignoreMessage) {
            console.warn(`Skipping record with no ${primaryKey} (${csvInputData && csvInputData.join()})`);
            return next();
        }

        this.wpeApi.get(`${DataFormat.apiProperties.endpointRelativePath}${primaryKeyValue}`)
            .then((apiData: any) => {
                if (apiData === '404' || !apiData) {
                    console.warn(`Warning - record for ${primaryKey} ${csvInputData[primaryKey]} not found at`);
                    console.warn(`${this.wpeApi.endpointBaseUrl}${DataFormat.apiProperties.endpointRelativePath}${primaryKeyValue}. This record will be incomplete`);
                    console.warn(`Please check the url above and your internet connectivity if this affects multiple records\r\n`);
                    this.setApiFields(csvInputData);
                } else {
                    this.setApiFields(csvInputData, apiData);
                }
                this.ensureOnlyOutputFields(csvInputData);
                next(null, csvInputData);
            });
    }

    /**
     * An attempt to merge fields without needing to hard-code any of the headers or api names
     *
     * @param {Object} csvInputData - the data fed to the transform (from csv parser in this case, might need a more generic name as it's altered by transform)
     * @param {Object} apiData - the data retrieved from the api or an empty object for easier assigning of apiValue below
     *
     */
    setApiFields(csvInputData: Object, apiData: Object = {}) {
        const apiSchema = DataFormat.apiProperties;
        apiSchema.keysToRetrieve.forEach((key: string) => {
            const csvPropertyName = apiSchema.mapping[key];
            const apiValue = apiData[key] || '';

            if (csvPropertyName) {
                csvInputData[csvPropertyName] = apiValue;
            }
        })
    }

    /**
     * Make sure that we're only passing the fields we want in our final output file, not needed for current data but it could easily be needed
     *
     * @param {Object} csvInputData - the data we have been manipulating in this transformer consisting of input csv fields and merged api fields
     */
    ensureOnlyOutputFields(csvInputData: Object) {
        const allowedOutputFields = [...DataFormat.csvFileHeaders.outputFileHeaders];
        Object.keys(csvInputData).forEach((key: string) => {
            const allowedFieldsIndex = allowedOutputFields.findIndex((value: string) => key === value);
            if (allowedFieldsIndex > -1) {
                //prevent duplicates
                delete (allowedOutputFields[allowedFieldsIndex])
            } else {
                delete csvInputData[key];
            }
        })
    }

}
