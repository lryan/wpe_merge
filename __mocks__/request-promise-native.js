/**
 * Jest mock class for request-promise-native
 *
 * @type {{}}
 */

const rpn = {};

rpn.get = (options) => {
    const response = {
        headers: {
            'content-type': 'application/json'
        },
        statusCode: 200,
        body: `{"uri": "${options.uri}"}`
    };

    switch (options.uri) {

        case('https://fake.endpoint.com/404'):
            return Promise.reject({statusCode: 404});

        case('https://fake.endpoint.com/403'):
            return Promise.reject({statusCode: 403});

        case('https://fake.endpoint.com/text/html'):
            response.headers = {'content-type': 'text/html'};
            break;

        case('https://fake.endpoint.com/emptyjson'):
            response.body = '{}';
            break;

        case('https://fake.endpoint.com/badjson'):
            response.body = '';
            break;

        default:
            response.body = `{"uri": "${options.uri}"}`;
            break;
    }

    return Promise.resolve(response);
};


module.exports = rpn;